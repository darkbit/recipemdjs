import { parseRecipe } from './parser.js'

console.log('Let the deno come!')
let input = new TextDecoder().decode(Deno.readFileSync(Deno.args[0]))
console.log(input)
let recipe = parseRecipe(input)
console.log(JSON.stringify(recipe))
