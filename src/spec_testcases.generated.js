export default {
	"recipe.md": {
		"content": "# Title\n\nThe description describes this recipe. It is delicious!\n\nIt can have multiple lines an may even include pictures.\n\n<img src=\"../logo/recipemd-mark.svg\" />\n\n*vegetarian, vegan, not a real recipe*\n\n**5 cups, 20 ml, 5.5 Tassen**\n\n---\n\n- *5* ungrouped ingredient\n- *5.2 ml* grouped ingredient\n\n## Group 1\n\n- *1* [link ingredient](./ingredients.md)\n- unit is optional\n\n### Subgroup 1.1\n\n- *1.25 ml* ingredient\n\n## Group 2\n\n- text isn't optional\n- *hey* amount is valid without factor\n\n---\n\nInstructions are very instructive.",
		"invalid": false,
		"expected": {
			"title": "Title",
			"description": "The description describes this recipe. It is delicious!\n\nIt can have multiple lines an may even include pictures.\n\n<img src=\"../logo/recipemd-mark.svg\" />",
			"yields": [
				{
					"factor": "5",
					"unit": "cups"
				},
				{
					"factor": "20",
					"unit": "ml"
				},
				{
					"factor": "5.5",
					"unit": "Tassen"
				}
			],
			"tags": [
				"vegetarian",
				"vegan",
				"not a real recipe"
			],
			"ingredients": [
				{
					"name": "ungrouped ingredient",
					"amount": {
						"factor": "5",
						"unit": null
					},
					"link": null
				},
				{
					"name": "grouped ingredient",
					"amount": {
						"factor": "5.2",
						"unit": "ml"
					},
					"link": null
				}
			],
			"ingredient_groups": [
				{
					"title": "Group 1",
					"ingredients": [
						{
							"name": "link ingredient",
							"amount": {
								"factor": "1",
								"unit": null
							},
							"link": "./ingredients.md"
						},
						{
							"name": "unit is optional",
							"amount": null,
							"link": null
						}
					],
					"ingredient_groups": [
						{
							"title": "Subgroup 1.1",
							"ingredients": [
								{
									"name": "ingredient",
									"amount": {
										"factor": "1.25",
										"unit": "ml"
									},
									"link": null
								}
							],
							"ingredient_groups": []
						}
					]
				},
				{
					"title": "Group 2",
					"ingredients": [
						{
							"name": "text isn't optional",
							"amount": null,
							"link": null
						},
						{
							"name": "amount is valid without factor",
							"amount": {
								"factor": null,
								"unit": "hey"
							},
							"link": null
						}
					],
					"ingredient_groups": []
				}
			],
			"instructions": "Instructions are very instructive."
		}
	},
	"commonmark_fenced_code_blocks.md": {
		"content": "# Bug\n\n   `````\n   abc\n   d\n   `````\n\n---\n\n- foo\n\n---\n\n   ```\n   abc\n   d\n```",
		"invalid": false,
		"expected": {
			"ingredients": [
				{
					"name": "foo",
					"amount": null,
					"link": null
				}
			],
			"ingredient_groups": [],
			"title": "Bug",
			"description": "   `````\n   abc\n   d\n   `````",
			"yields": [],
			"tags": [],
			"instructions": "   ```\n   abc\n   d\n```"
		}
	},
	"empty.invalid.md": {
		"content": "",
		"invalid": true
	},
	"ingredients.md": {
		"content": "# Recipe with ingredients\n\n---\n\n- *20 ml* water\n- *1 cup* earl grey, hot\n- *1 1/2 cup* coffee\n- *¼ kg* cheese\n- salt\n- ingredients may contain *markdown*",
		"invalid": false,
		"expected": {
			"title": "Recipe with ingredients",
			"description": null,
			"yields": [],
			"tags": [],
			"ingredients": [
				{
					"name": "water",
					"amount": {
						"factor": "20",
						"unit": "ml"
					},
					"link": null
				},
				{
					"name": "earl grey, hot",
					"amount": {
						"factor": "1",
						"unit": "cup"
					},
					"link": null
				},
				{
					"name": "coffee",
					"amount": {
						"factor": "1.5",
						"unit": "cup"
					},
					"link": null
				},
				{
					"name": "cheese",
					"amount": {
						"factor": "0.25",
						"unit": "kg"
					},
					"link": null
				},
				{
					"name": "salt",
					"amount": null,
					"link": null
				},
				{
					"name": "ingredients may contain *markdown*",
					"amount": null,
					"link": null
				}
			],
			"ingredient_groups": [],
			"instructions": null
		}
	},
	"ingredients_empty.invalid.md": {
		"content": "# The Empty Ingredient\n\n---\n\n-   \n\n---",
		"invalid": true
	},
	"ingredients_groups.md": {
		"content": "# Recipe with ingredient groups\n\n---\n\n- ingredient 0\n\n## Group 1\n\n- ingredient 1\n- ingredient 2\n\n### Subgroup 1.1\n\n- ingredient 3\n- ingredient 4\n\n##### Subgroup 1.1.1 (Two level deeper headline, still one lever deeper group)\n\n- ingredient 5\n\n#### Subgroup 1.1.2 (One level deeper headline)\n\n- ingredient 6\n\n# Group 2\n\n- ingredient 7\n- ingredient 8",
		"invalid": false,
		"expected": {
			"title": "Recipe with ingredient groups",
			"description": null,
			"yields": [],
			"tags": [],
			"ingredients": [
				{
					"name": "ingredient 0",
					"amount": null,
					"link": null
				}
			],
			"ingredient_groups": [
				{
					"title": "Group 1",
					"ingredients": [
						{
							"name": "ingredient 1",
							"amount": null,
							"link": null
						},
						{
							"name": "ingredient 2",
							"amount": null,
							"link": null
						}
					],
					"ingredient_groups": [
						{
							"title": "Subgroup 1.1",
							"ingredients": [
								{
									"name": "ingredient 3",
									"amount": null,
									"link": null
								},
								{
									"name": "ingredient 4",
									"amount": null,
									"link": null
								}
							],
							"ingredient_groups": [
								{
									"title": "Subgroup 1.1.1 (Two level deeper headline, still one lever deeper group)",
									"ingredients": [
										{
											"name": "ingredient 5",
											"amount": null,
											"link": null
										}
									],
									"ingredient_groups": []
								},
								{
									"title": "Subgroup 1.1.2 (One level deeper headline)",
									"ingredients": [
										{
											"name": "ingredient 6",
											"amount": null,
											"link": null
										}
									],
									"ingredient_groups": []
								}
							]
						}
					]
				},
				{
					"title": "Group 2",
					"ingredients": [
						{
							"name": "ingredient 7",
							"amount": null,
							"link": null
						},
						{
							"name": "ingredient 8",
							"amount": null,
							"link": null
						}
					],
					"ingredient_groups": []
				}
			],
			"instructions": null
		}
	},
	"ingredients_links.md": {
		"content": "# Ingredients with Links\n\n---\n\n- [link to something](./another_recipe.md)\n\n- [link to something else](http://example.org)\n\n- *5 ml* [link to something with amount](http://example.org)",
		"invalid": false,
		"expected": {
			"title": "Ingredients with Links",
			"description": null,
			"yields": [],
			"tags": [],
			"ingredients": [
				{
					"name": "link to something",
					"amount": null,
					"link": "./another_recipe.md"
				},
				{
					"name": "link to something else",
					"amount": null,
					"link": "http://example.org"
				},
				{
					"name": "link to something with amount",
					"amount": {
						"factor": "5",
						"unit": "ml"
					},
					"link": "http://example.org"
				}
			],
			"ingredient_groups": [],
			"instructions": null
		}
	},
	"ingredients_multiline.md": {
		"content": "# Multiline Ingredients\n\n---\n\n- This is a long ingredient\n\n  it spans multiple paragraphs\n  \n  this may be unusual but is valid\n  \n  \n- this is a normal boring ingredient\n\n\n- *5ml* is an amount that\n\n  is the amount of *this* ingredient\n  \n- *5 grams*\n  \n  is an amount that stands alone on the first line\n  \n- [this is a link](some other recipe)\n\n\n- [this is not a link](some other recipe) \n\n  since links must wrap the whole ingredient text",
		"invalid": false,
		"expected": {
			"title": "Multiline Ingredients",
			"description": null,
			"yields": [],
			"tags": [],
			"ingredients": [
				{
					"name": "This is a long ingredient\n\n  it spans multiple paragraphs\n  \n  this may be unusual but is valid",
					"amount": null,
					"link": null
				},
				{
					"name": "this is a normal boring ingredient",
					"amount": null,
					"link": null
				},
				{
					"name": "is an amount that\n\n  is the amount of *this* ingredient",
					"amount": {
						"factor": "5",
						"unit": "ml"
					},
					"link": null
				},
				{
					"name": "is an amount that stands alone on the first line",
					"amount": {
						"factor": "5",
						"unit": "grams"
					},
					"link": null
				},
				{
					"name": "[this is a link](some other recipe)",
					"amount": null,
					"link": null
				},
				{
					"name": "[this is not a link](some other recipe)\n\n  since links must wrap the whole ingredient text",
					"amount": null,
					"link": null
				}
			],
			"ingredient_groups": [],
			"instructions": null
		}
	},
	"ingredients_no_divider.invalid.md": {
		"content": "# Title is okay\n\nThis is a description and there is no ingredient divider",
		"invalid": true
	},
	"ingredients_no_name.invalid.md": {
		"content": "# The Nameless Ingredient\n\n---\n\n- *5 nothings*  \n\n---",
		"invalid": true
	},
	"instructions.md": {
		"content": "# Instructions\n\n---\n\n---\n\nThe instructions are not processed in any way.\n\n## Can they contain markdown?\n\nYes, absolutely!",
		"invalid": false,
		"expected": {
			"title": "Instructions",
			"description": null,
			"yields": [],
			"tags": [],
			"ingredients": [],
			"ingredient_groups": [],
			"instructions": "The instructions are not processed in any way.\n\n## Can they contain markdown?\n\nYes, absolutely!"
		}
	},
	"instructions_no_divider.invalid.md": {
		"content": "# Title is good\n\n---\n\nA paragraph is not valid in the ingredients section, so we're missing the divider before instructions.",
		"invalid": true
	},
	"tags.md": {
		"content": "# Tags\n\n*tag1, tag2, tag3, tag4, tag with special! char, tag5*\n\n---\n\n",
		"invalid": false,
		"expected": {
			"title": "Tags",
			"description": null,
			"yields": [],
			"tags": [
				"tag1",
				"tag2",
				"tag3",
				"tag4",
				"tag with special! char",
				"tag5"
			],
			"ingredients": [],
			"ingredient_groups": [],
			"instructions": null
		}
	},
	"tags_multiple.invalid.md": {
		"content": "# Multiple Tag Paragraphs\n\nA recipe may specify tags only once\n\n*tag1, tag2, tag3, tag4, tag5*\n\n*more tags!, how's that supposed to work?*\n\n---\n\n",
		"invalid": true
	},
	"tags_splitting.md": {
		"content": "# Tags splitting\n\nCommas are not a tag separator when between two numbers\n\n*tag1,1, tag1,2, tag1,3*\n\n---",
		"invalid": false,
		"expected": {
			"ingredients": [],
			"ingredient_groups": [],
			"title": "Tags splitting",
			"description": "Commas are not a tag separator when between two numbers",
			"yields": [],
			"tags": [
				"tag1,1",
				"tag1,2",
				"tag1,3"
			],
			"instructions": null
		}
	},
	"tags_yields.md": {
		"content": "# Tags then Yields\n\n*tag1, tag2, tag3, tag4, tag with special! char, tag5*\n\n**1.2 cups, 1,5 Tassen, 1 1/4 servings, 5 servings, factorless, 5**\n\n---\n\n",
		"invalid": false,
		"expected": {
			"title": "Tags then Yields",
			"description": null,
			"yields": [
				{
					"factor": "1.2",
					"unit": "cups"
				},
				{
					"factor": "1.5",
					"unit": "Tassen"
				},
				{
					"factor": "1.25",
					"unit": "servings"
				},
				{
					"factor": "5",
					"unit": "servings"
				},
				{
					"factor": null,
					"unit": "factorless"
				},
				{
					"factor": "5",
					"unit": null
				}
			],
			"tags": [
				"tag1",
				"tag2",
				"tag3",
				"tag4",
				"tag with special! char",
				"tag5"
			],
			"ingredients": [],
			"ingredient_groups": [],
			"instructions": null
		}
	},
	"title.md": {
		"content": "# The Most Useless Recipe\n\n---",
		"invalid": false,
		"expected": {
			"title": "The Most Useless Recipe",
			"description": null,
			"yields": [],
			"tags": [],
			"ingredients": [],
			"ingredient_groups": [],
			"instructions": null
		}
	},
	"title_second_level_heading.invalid.md": {
		"content": "## This is not a valid title",
		"invalid": true
	},
	"yields.md": {
		"content": "# Yields\n\n**1.2 cups, 1,5 Tassen, 1 1/4 servings, 5 servings, factorless, 5**\n\n---",
		"invalid": false,
		"expected": {
			"title": "Yields",
			"description": null,
			"yields": [
				{
					"factor": "1.2",
					"unit": "cups"
				},
				{
					"factor": "1.5",
					"unit": "Tassen"
				},
				{
					"factor": "1.25",
					"unit": "servings"
				},
				{
					"factor": "5",
					"unit": "servings"
				},
				{
					"factor": null,
					"unit": "factorless"
				},
				{
					"factor": "5",
					"unit": null
				}
			],
			"tags": [],
			"ingredients": [],
			"ingredient_groups": [],
			"instructions": null
		}
	},
	"yields_multiple.invalid.md": {
		"content": "# Multiple Yield Paragraphs\n\n**1,5 Tassen**\n\n**2 Portionen**\n\n---",
		"invalid": true
	},
	"yields_tags.md": {
		"content": "# Yields Then Tags\n\n**1.2 cups, 1,5 Tassen, 1 1/4 servings, 5 servings, factorless, 5**\n\n*tag1, tag2, tag3, tag4, tag with special! char, tag5*\n\n---\n\n",
		"invalid": false,
		"expected": {
			"title": "Yields Then Tags",
			"yields": [
				{
					"factor": "1.2",
					"unit": "cups"
				},
				{
					"factor": "1.5",
					"unit": "Tassen"
				},
				{
					"factor": "1.25",
					"unit": "servings"
				},
				{
					"factor": "5",
					"unit": "servings"
				},
				{
					"factor": null,
					"unit": "factorless"
				},
				{
					"factor": "5",
					"unit": null
				}
			],
			"tags": [
				"tag1",
				"tag2",
				"tag3",
				"tag4",
				"tag with special! char",
				"tag5"
			],
			"ingredients": [],
			"ingredient_groups": [],
			"description": null,
			"instructions": null
		}
	}
}