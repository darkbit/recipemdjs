import { Parser } from 'https://esm.sh/commonmark@0.30.0'

export function parseRecipe(input) {
	let tree = new Parser().parse(input)
	let splitInput = input.split('\n')

	expect(tree.type, 'document')

	let it = nodeIt(tree.firstChild)

	let recipe = {}

	let title = it.next().value
	expect(title.type, 'heading')
	expect(title.level, 1)
	recipe.title = title.firstChild.literal

	let startdesc, enddesc
	let foundbreak = false
	for(let cur = it.next().value; cur; cur = it.next().value) {
		if(cur.type == 'thematic_break') {
			foundbreak = true
			break
		}

		// Anything can be here... expect(cur.type, [ 'paragraph', 'html_block', 'code_block' ])

		if(cur.firstChild == cur.lastChild && cur.firstChild) {
			if(cur.firstChild.type == 'strong') {
				if(recipe.yields)
					throw new Error('multiple yields are not allowed!')
				recipe.yields = parseYields(textContent(cur.firstChild))
				continue
			} else if(cur.firstChild.type == 'emph') {
				if(recipe.tags)
					throw new Error('multiple tags are not allowed!')
				recipe.tags = parseTags(textContent(cur.firstChild))
				continue
			}
		}

		if(recipe.tags || recipe.yields)
			throw new Error('description found after the tags and yields!')

		if(!startdesc)
			startdesc = cur.sourcepos[0]
		enddesc = cur.sourcepos[1]
	}
	if(!foundbreak)
		throw new Error('missing a thematic break for the ingredients')

	if(!recipe.tags)
		recipe.tags = []
	if(!recipe.yields)
		recipe.yields = []

	if(startdesc)
		recipe.description = strFromBoundaries(splitInput, [startdesc, enddesc])
	else
		recipe.description = null

	let groupStack = [{
		title: "fake top level group",
		ingredients: [],
		ingredient_groups: [],
		level: -1
	}]
	for(let cur = it.next().value; cur; cur = it.next().value) {
		if(cur.type == 'thematic_break')
			break

		if(cur.type == 'heading') {
			let level = cur.level
			while(level <= groupStack[0].level)
				groupStack.shift()
			let group = {
				title: cur.firstChild.literal,
				ingredients: [],
				ingredient_groups: [],
				level: level
			}
			groupStack[0].ingredient_groups.push(group)
			groupStack.unshift(group)
			continue
		}

		expect(cur.type, 'list')
		for(let ing of nodeIt(cur.firstChild)) {
			expect(ing.type, 'item')
			expect(ing.firstChild.type, 'paragraph')
			let ingredient = {}
			ingredient.name = strFromBoundaries(splitInput, [ing.firstChild.sourcepos[0], ing.lastChild.sourcepos[1]])
			let next = ing.firstChild.firstChild
			if(next.type == 'emph') {
				try {
				ingredient.amount = parseAmount(next.firstChild.literal)
				next = next.next
				} catch(e) {}
			}
			// drop whitespaces before a possible link
			if(next && next.type == 'text' && next.literal.trim().length == 0)
				next = next.next

			if(next && next.type == 'link' && ing.firstChild.lastChild == next) {
				ingredient.link = next.destination
			}

			// necessary, as sourcepos doesn't exist for inline elements
			if(ingredient.amount)
				ingredient.name = ingredient.name.split('*').slice(2).join('*')
			else
				ingredient.amount = null

			if(ingredient.link)
				ingredient.name = /^\s*\[(.*)\]\(/.exec(ingredient.name)[1]
			else
				ingredient.link = null

			ingredient.name = ingredient.name.trim()
			if(ingredient.name == '')
				throw new Error('all ingredients must have a name')

			groupStack[0].ingredients.push(ingredient)
		}
	}
	let topgrp = groupStack.pop()
	recipe.ingredients = topgrp.ingredients
	if(topgrp.ingredient_groups.length > 0) {
		let stripGroup = group => {
			if(group.ingredient_groups.length == 0)
				delete group.ingredient_groups
			else {
				for(let subgroup of group.ingredient_groups)
					stripGroup(subgroup)
			}
			delete group.level
		}
		stripGroup(topgrp)
		recipe.ingredient_groups = topgrp.ingredient_groups
	}

	let inststart = it.next().value
	if(inststart) {
		recipe.instructions = strFromBoundaries(splitInput, [inststart.sourcepos[0], tree.lastChild.sourcepos[1]])
	} else
		recipe.instructions = null

	return recipe
}

function strFromBoundaries(splitinput, boundaries) {
	let ret = splitinput.slice(boundaries[0][0] - 1, boundaries[1][0]).join('\n')
	let missingend = boundaries[1][1] - splitinput[boundaries[1][0] - 1].length
	if(missingend == 0)
		missingend = undefined
	return ret.slice(boundaries[0][1] - 1, missingend)
}

function amountListSplit(input) {
	let splitted = input.split(',')
	for(let i = 0; i < splitted.length; i++) {
		if(/\d$/.test(splitted[i]) && /^\d/.test(splitted[i+1])) {
			splitted[i] += ',' + splitted[i+1]
			splitted.splice(i + 1, 1)
		}
	}
	return splitted
}

function parseTags(input) {
	return amountListSplit(input).map(tag => tag.trim())
}

function parseAmount(input) {
	input = input.trim()
	let ret = {}
	let res
	if(res = /^(\d+\s)?(\d+)\/(\d+)(.+)?$/.exec(input)) {
		// Proper or improper fraction...
		ret.factor = res[2] / res[3]
		if(res[1])
			ret.factor += +res[1]
		ret.factor += ''

		if(res[4])
			ret.unit = res[4]
	} else if(res = /^(\d+([\.,]\d+)?)(.+)?$/.exec(input)) {
		// Simple decimal numbers
		ret.factor = res[1].replace(',', '.')
		if(res[3])
			ret.unit = res[3]
	} else {
		// now only unicode vulgar fractions are possible
		switch(input[0]) {
			case '¼': ret.factor = '0.25';
				break
			case '½': ret.factor = '0.5';
				break
			case '¾': ret.factor = '0.75';
				break
			case '⅐': ret.factor = '0.14285714285714285';
				break
			case '⅑': ret.factor = '0.11111111111111111';
				break
			case '⅒': ret.factor = '0.1';
				break
			case '⅓': ret.factor = '0.33333333333333333';
				break
			case '⅔': ret.factor = '0.66666666666666666';
				break
			case '⅕': ret.factor = '0.2';
				break
			case '⅖': ret.factor = '0.4';
				break
			case '⅗': ret.factor = '0.6';
				break
			case '⅘': ret.factor = '0.8';
				break
			case '⅙': ret.factor = '0.166666666666666666';
				break
			case '⅚': ret.factor = '0.833333333333333333';
				break
			case '⅛': ret.factor = '0.125';
				break
			case '⅜': ret.factor = '0.375';
				break
			case '⅝': ret.factor = '0.625';
				break
			case '⅞': ret.factor = '0.875';
				break
			default: // throw `missing value in amount: ${input}`;
				// TODO currently a bug, for the testcases we allow factorless things: https://github.com/tstehr/RecipeMD/issues/38
				ret.unit = input
		}
		if(ret.factor && input.length > 1) {
			ret.unit = input.slice(1)
		}
	}

	// TODO should the spec clarify the spaces?
	if(ret.unit)
		ret.unit = ret.unit.trim()
	else
		ret.unit = null
	// TODO affected by the above issue
	if(!ret.factor)
		ret.factor = null

	return ret
}

function textContent(node) {
	let ret = ''
	for(let child of nodeIt(node.firstChild)) {
		ret += child.literal
	}
	return ret
}

function parseYields(input) {
	return amountListSplit(input).map(parseAmount)
}


function* nodeIt(startNode) {
	for(let curNode = startNode; curNode; curNode = curNode.next) {
		yield curNode
	}
}

function expect(val, expected) {
	if(!Array.isArray(expected))
		expected = [ expected ]
	if (!expected.includes(val)) {
		throw new Error(`Expected ${expected}, but got ${val}`)
	}
}
