import { parseRecipe } from './parser.js'
import { assertEquals, assertThrows } from 'https://deno.land/std@0.106.0/testing/asserts.ts';
import testcases from './spec_testcases.generated.js'

for(const [name, testcase] of Object.entries(testcases)) {
	Deno.test(
		name,
		testcase.invalid ?
			() => assertThrows(() => parseRecipe(testcase.content)) :
			() => assertEquals(parseRecipe(testcase.content), fixTestcase(testcase.expected))
	)
}

// Fixing non deterministic fields in testcases
function fixTestcase(testcase) {
	if(testcase?.ingredient_groups?.length == 0)
		delete testcase.ingredient_groups
	else if(testcase.ingredient_groups)
		testcase.ingredient_groups = testcase.ingredient_groups.map(fixTestcase)

	return testcase
}
