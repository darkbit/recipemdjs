const baseDir = 'RecipeMD/testcases'
const goal = 'src/spec_testcases.generated.js'

await Deno.permissions.request({ name: 'read', path: baseDir })
await Deno.permissions.request({ name: 'write', path: goal })

let testcases = {}

for await (const entry of Deno.readDir('RecipeMD/testcases')) {
	if(!entry.name.endsWith('.md') || !entry.isFile)
		continue

	let testcase = {
		content: await Deno.readTextFile(baseDir + '/' + entry.name),
		invalid: entry.name.endsWith('.invalid.md')
	}

	if(!testcase.invalid)
		testcase.expected = JSON.parse(await Deno.readTextFile(baseDir + '/' + entry.name.slice(0, -2) + 'json'))

	testcases[entry.name] = testcase
}

// Not quite right, as JSON is not always valid JS, we shouldn't run into this small difference
await Deno.writeTextFile(goal, 'export default ' + JSON.stringify(testcases, null, '\t'))
