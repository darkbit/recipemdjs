LOCKFILE = lock.json
DENORUN = deno run --lock=${LOCKFILE} --cached-only

JSFILES=$(shell find . -name '*.js')

cache:
	deno cache --lock=${LOCKFILE} ${JSFILES}

updatecache:
	deno cache --lock=${LOCKFILE} --lock-write ${JSFILES}

test: cache
	deno test --lock=${LOCKFILE} --cached-only
